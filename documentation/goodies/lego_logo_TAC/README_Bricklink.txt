Pour ouvrir le fichier Bricklink_TousAntiCovid.io avec le logiciel BrickLink STUDIO 2.0:

• Aller sur https://www.bricklink.com/v3/studio/download.page pour télécharger BrickLink STUDIO 2.0.
• Installer BrickLink STUDIO
• Ouvrir BrickLink STUDIO
• De l'écran d'accueil, cliquer sur “OUVRIR”. Sinon à partir du menu, Fichier > Ouvrir
• Sélectionner le fichier “Bricklink_TousAntiCovid.io”